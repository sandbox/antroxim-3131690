(function ($, Drupal) {

  'use strict';

  /**
   * Override drupal selectHandler function
   * @link https://drupal.stackexchange.com/questions/263778/autocomplete-textfield-displays-value-when-selected-and-not-label
   */
  function customSelectHandler(event, ui) {
    var valueField = $(event.target);
    var valueFieldDrupalSelector = event.target.dataset.drupalSelector.replace('-name', '-ref');

    if ($('[data-drupal-selector=' + valueFieldDrupalSelector + ']').length > 0) {
      valueField = $('[data-drupal-selector=' + valueFieldDrupalSelector + ']');
      // update the labels too
      const labels = Drupal.autocomplete.splitValues(event.target.value);
      labels.pop();
      labels.push(ui.item.label);
      event.target.value = labels.join(', ');
    }
    const terms = Drupal.autocomplete.splitValues(valueField.val());
    // Remove the current input.
    terms.pop();
    // Add the selected item.
    terms.push(ui.item.value);

    valueField.val(terms.join(', '));
    // Return false to tell jQuery UI that we've filled in the value already.
    return false;
  }

  Drupal.behaviors.CommerceNpAutocomplete = {
    attach: function (context, settings) {
      // attach custom select handler to fields with class
      $('input.commerce-np-autocomplete').autocomplete({
        select: customSelectHandler,
      });
    }
  };

})(jQuery, Drupal);
