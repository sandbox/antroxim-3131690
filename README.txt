Commerce NP module
Integration with popular Ukrainian delivery service "Nova Poshta"
Provides settings page to configure sender warehouse and a field type to store delivery type and address.

Configuration page
/admin/commerce/config/commerce_np

Working features:

Connects with Nova Poshta API to search trough their address database (City, Warehouse)
Selection of 1/3 types of delivery (to warehouse, address and Post-o-mat)

Features under development:
Additional field for admins to register delivery and get tracking number
Set of events to trigger on delivery status change (on cron)

Dependencies:
https://www.drupal.org/project/select2
