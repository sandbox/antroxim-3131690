<?php

namespace Drupal\commerce_np\Form;

use Drupal\commerce_np\CommerceNpConnect;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ConfigForm.
 */
class ConfigForm extends ConfigFormBase {

  /**
   * Nova poshta API service.
   *
   * @var CommerceNpConnect $npConnect
   */
  protected $npConnect;

  /**
   * Class constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   * @param \Drupal\commerce_np\CommerceNpConnect $np_connect
   */
  public function __construct(
    ConfigFactoryInterface $config_factory, CommerceNpConnect $np_connect) {
    parent::__construct($config_factory);
    $this->npConnect = $np_connect;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('commerce_np.connect')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'commerce_np.default',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('commerce_np.default');
    $form['#attached']['library'][] = 'commerce_np/commerce_np_autocomplete';

    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API key'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('api_key'),
    ];


    $np_sender = $config->get('np_sender') ? $config->get('np_sender') : [];
    $form['np_sender'] = [
      '#type' => 'fieldset',
      '#tree' => TRUE,
      '#title' => $this->t('Sender details'),
      '#attributes' => [
        'style' => 'width: 31em;',
      ],
    ];
    $form['np_sender']['first_name'] = [
      '#type' => 'textfield',
      '#title' => t('First name'),
      '#default_value' => !empty($np_sender['first_name']) ? $np_sender['first_name'] : '',
    ];
    $form['np_sender']['last_name'] = [
      '#type' => 'textfield',
      '#title' => t('Last name'),
      '#default_value' => !empty($np_sender['last_name']) ? $np_sender['last_name'] : '',
    ];
    $form['np_sender']['phone_number'] = [
      '#type' => 'textfield',
      '#title' => t('Phone number'),
      '#default_value' => !empty($np_sender['phone_number']) ? $np_sender['phone_number'] : '',
    ];
    $form['np_sender']['city_name'] = [
      '#type' => 'textfield',
      '#title' => t('City'),
      '#attributes' => ['class' => ['commerce-np-autocomplete']],
      '#autocomplete_route_name' => 'commerce_np.autocomplete',
      '#autocomplete_route_parameters' => ['type' => 'city'],
      '#default_value' => !empty($np_sender['city_name']) ? $np_sender['city_name'] : '',
      '#ajax' => [
        'callback' => '::ajaxUpdateWarehouses',
        'disable-refocus' => TRUE,
        'event' => 'autocompleteclose',
        'wrapper' => 'edit-warehouse',
      ],
    ];

    // City field value.
    $form['np_sender']['city_ref'] = [
      '#type' => 'hidden',
      '#default_value' => !empty($np_sender['city_ref']) ? $np_sender['city_ref'] : '',
    ];

    //@TODO: Check if select2 module exists.
    $options = [];
    $cityref = NULL;
    if (!$cityref = $form_state->getValue(['np_sender', 'city_ref'])) {
      $cityref = !empty($np_sender['city_ref']) ? $np_sender['city_ref'] : '';
    }

    if ($cityref) {
      $warehouses = $this->npConnect->getWarehouses($cityref);
      foreach ($warehouses as $warehouse) {
        $options[$warehouse['Ref']] = '№ ' . $warehouse['Number'] . ' ' . $warehouse['ShortAddress'];
      }
    }

    $form['np_sender']['warehouse_ref'] = [
      '#type' => 'select2',
      '#title' => t('Warehouse'),
      '#default_value' => !empty($np_sender['warehouse_ref']) ? $np_sender['warehouse_ref'] : '',
      '#empty_option' => '- Select City first -',
      '#options' => $options,
      '#prefix' => '<div id="edit-warehouse">',
      '#suffix' => '</div>',
      '#attributes' => [
        'style' => 'width: 31em;',
      ],
    ];


    return parent::buildForm($form, $form_state);
  }

  /**
   * AJAX handler for warehouses select list.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return mixed Form element.
   */
  public function ajaxUpdateWarehouses(
    array &$form, FormStateInterface $form_state) {
    return $form['np_sender']['warehouse_ref'];
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('commerce_np.default')
         ->set('api_key', $form_state->getValue('api_key'))
         ->set('np_sender', $form_state->getValue('np_sender'))
         ->save();

  }

}
