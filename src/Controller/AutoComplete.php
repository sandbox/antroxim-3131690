<?php

namespace Drupal\commerce_np\Controller;

use Drupal\commerce_np\CommerceNpConnect;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Component\Utility\Html;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Autocomplete callbacks for multiple form elements.
 */
class AutoComplete extends ControllerBase implements
  ContainerInjectionInterface {

  /**
   * Nova poshta API service.
   *
   * @var CommerceNpConnect $npConnect
   */
  protected $np;

  /**
   * Class constructor.
   *
   * @param \Drupal\commerce_np\CommerceNpConnect $np_connect
   */
  public function __construct(CommerceNpConnect $np_connect) {
    $this->np = $np_connect;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('commerce_np.connect')
    );
  }

  /**
   * Handle autocomplete requests.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   * @param string $type
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse|\Symfony\Component\HttpKernel\Exception\NotFoundHttpException
   */
  public function handleAutocomplete(Request $request, string $type) {
    switch ($type) {
      case 'city':
        return $this->CitySuggestions($request);
        break;
      default:
        return new NotFoundHttpException();
        break;
    }
  }

  /**
   * Handler for autocomplete request.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   */
  private function CitySuggestions(Request $request) {
    $results = [];
    $input = $request->query->get('q');
    $input = Xss::filter($input);
    $cities = $this->np->getCities($input);

    foreach ($cities as $item) {
      $label = Html::escape($item['Description'] . ' (' . $item['SettlementTypeDescription'] . ')');
      $results[] = [
        'value' => $item['Ref'],
        'label' => $label,
      ];
    }

    return new JsonResponse($results);
  }

}
