<?php

namespace Drupal\commerce_np;


use Drupal\Component\Serialization\Json;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Http\ClientFactory;
use Drupal\Core\Messenger\MessengerInterface;

/**
 * Class CommerceNpConnect.
 */
class CommerceNpConnect {

  const HTTP_ENDPOINT = 'https://api.novaposhta.ua/v2.0/json/';

  /**
   * The ACME Services - Contents HTTP Client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * @var  \Drupal\Core\Messenger\MessengerInterface
   */
  protected $msg;

  /**
   * @var CacheBackendInterface $cache
   */
  protected $cache;


  /**
   * Constructs a new CommerceNpConnect object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   * @param \Drupal\Core\Http\ClientFactory $http_client_factory
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   */
  public function __construct(
    ConfigFactoryInterface $configFactory,
    ClientFactory $http_client_factory,
    MessengerInterface $messenger,
    CacheBackendInterface $cache_backend) {
    $this->config = $configFactory->get('commerce_np.default');
    $this->httpClient = $http_client_factory->fromOptions();
    $this->msg = $messenger;
    $this->cache = $cache_backend;
  }

  /**
   * Makes request to NP API.
   *
   * @param $api_request
   *
   * @return mixed Body of request results.
   */
  private function getData(array $api_request) {
    $request_options = [
      'verify' => FALSE,
      'headers' => [
        'Content-type' => 'application/json',
      ],
    ];
    $request_options['json'] = $api_request;

    //Checking if data available in cache.
    $cid = 'commerce_np_data_cache_' . md5(serialize($api_request));
    if ($cached = $this->cache->get($cid)) {

      return $cached->data;
    }

    $request_options['json']['apiKey'] = $this->config->get('api_key');
    $request = $this->httpClient->post(self::HTTP_ENDPOINT, $request_options);

    //@TODO: Implement error handling and logging.
    $result = Json::decode($request->getBody());
    if ($result && $result['success']) {
      $expire = strtotime('now') + 60 * 60 * 24; // Refresh every day.
      $this->cache->set($cid, $result['data'], $expire);
      return $result['data'];
    }
    return FALSE;
  }

  /**
   * Test connection.
   */
  public function testConnection() {
    $options = [
      'modelName' => 'Address',
      'calledMethod' => 'searchSettlements',
      'methodProperties' => [
        'CityName' => 'київ',
        'limit' => 5,
      ],
    ];
    return (bool) $this->getData($options);
  }

  /**
   * Gets list of Cities with Warehouses using filter.
   *
   * @param string $filter
   *
   * @return mixed
   */
  public function getCities(string $filter) {
    $options = [
      'modelName' => 'Address',
      'calledMethod' => 'getCities',
      'methodProperties' => [
        'FindByString' => $filter,
      ],
    ];
    return $this->getData($options);
  }

  /**
   * Gets list of Warehouses by city ref number.
   *
   * @param string $filter
   *
   * @return mixed
   */
  public function getWarehouses(string $ref) {
    $options = [
      'modelName' => 'Address',
      'calledMethod' => 'getWarehouses',
      'methodProperties' => [
        'CityRef' => $ref,
      ],
    ];
    return $this->getData($options);
  }

  /**
   * Gets list of Streets by City ref.
   *
   * @param string $filter
   *
   * @return mixed
   */
  public function getStreets(string $city_ref, string $filter) {
    $options = [
      'modelName' => 'Address',
      'calledMethod' => 'searchSettlementStreets',
      'methodProperties' => [
        'StreetName' => $filter,
        'CityRef' => $city_ref,
        'Limit' => 10
      ],
    ];
    return $this->getData($options);
  }


}
