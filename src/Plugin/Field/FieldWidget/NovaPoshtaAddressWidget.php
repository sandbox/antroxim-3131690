<?php

namespace Drupal\commerce_np\Plugin\Field\FieldWidget;

use Drupal\commerce_np\CommerceNpConnect;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'nova_poshta_address_widget' widget.
 *
 * @FieldWidget(
 *   id = "nova_poshta_address_widget",
 *   module = "commerce_np",
 *   label = @Translation("Nova poshta address widget"),
 *   field_types = {
 *     "nova_poshta_address_type"
 *   }
 * )
 */
class NovaPoshtaAddressWidget extends WidgetBase implements ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * Nova poshta API service.
   *
   * @var CommerceNpConnect $npConnect
   */
  protected $npConnect;


  /**
   * @inheritDoc
   */
  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    array $third_party_settings,
    CommerceNpConnect $np_connect) {

    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);

    $this->npConnect = $np_connect;

  }

  /**
   * @inheritDoc
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('commerce_np.connect')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    $field_name = $this->fieldDefinition->getName();
    $element['#type'] = 'fieldset';
    $element['#attached']['library'][] = 'commerce_np/commerce_np_autocomplete';

    $element['type'] = [
      '#type' => 'radios',
      '#title' => $this->t('Delivery type'),
      '#options' => [
        0 => $this->t('Warehouse'),
        1 => $this->t('Postomat'),
        2 => $this->t('Address'),
      ],
      '#default_value' => isset($items[$delta]->type) ? $items[$delta]->type : 0,
    ];

    $element['first_name'] = [
      '#type' => 'textfield',
      '#title' => t('First name'),
//      '#required' => TRUE,
      '#size' => 20,
      '#default_value' => isset($items[$delta]->first_name) ? $items[$delta]->first_name : NULL,
    ];

    $element['last_name'] = [
      '#type' => 'textfield',
      '#title' => t('Last name'),
//      '#required' => TRUE,
      '#size' => 30,
      '#default_value' => isset($items[$delta]->last_name) ? $items[$delta]->last_name : NULL,
    ];
    $element['phone_number'] = [
      '#type' => 'textfield',
      '#title' => t('Phone number'),
      '#required' => TRUE,
      '#size' => 30,
      '#default_value' => isset($items[$delta]->phone_number) ? $items[$delta]->phone_number : NULL,
    ];

    $element['city_name'] = [
      '#type' => 'textfield',
      '#title' => t('City'),
//      '#required' => TRUE,
      '#attributes' => ['class' => ['commerce-np-autocomplete']],
      '#autocomplete_route_name' => 'commerce_np.autocomplete',
      '#autocomplete_route_parameters' => ['type' => 'city'],
      '#default_value' => isset($items[$delta]->city_name) ? $items[$delta]->city_name : NULL,
      '#ajax' => [
        'callback' => [$this, 'ajaxUpdateWarehouses'],
        'disable-refocus' => TRUE,
        'event' => 'autocompleteclose',
        'wrapper' => 'edit-warehouse-' . $delta,
      ],
    ];
    // City ref field value.
    $element['city_ref'] = [
      '#type' => 'hidden',
      '#default_value' => isset($items[$delta]->city_ref) ? $items[$delta]->city_ref : NULL,
    ];

    $element['street_name'] = [
      '#type' => 'textfield',
      '#title' => t('Street name'),
      '#default_value' => isset($items[$delta]->street_name) ? $items[$delta]->street_name : NULL,
      '#states' => [
        'visible' => [
          ':input[name="' . $field_name . '[' . $delta . '][type]"]' => ['value' => '2'],
        ],
        'required' => [
          ':input[name="' . $field_name . '[' . $delta . '][type]"]' => ['value' => '2'],
        ],
      ],
    ];
    $element['house_num'] = [
      '#type' => 'textfield',
      '#title' => t('House number'),
      '#size' => 10,
      '#default_value' => isset($items[$delta]->house_num) ? $items[$delta]->house_num : NULL,
      '#states' => [
        'visible' => [
          ':input[name="' . $field_name . '[' . $delta . '][type]"]' => ['value' => '2'],
        ],
        'required' => [
          ':input[name="' . $field_name . '[' . $delta . '][type]"]' => ['value' => '2'],
        ],
      ],
    ];
    $element['apt_num'] = [
      '#type' => 'textfield',
      '#title' => t('Apartment'),
      '#size' => 10,
      '#default_value' => isset($items[$delta]->apt_num) ? $items[$delta]->apt_num : NULL,
      '#states' => [
        'visible' => [
          ':input[name="' . $field_name . '[' . $delta . '][type]"]' => ['value' => '2'],
        ],
      ],
    ];

    $cityref = NULL;
    if (!$cityref = $form_state->getValue([$field_name, $delta, 'city_ref'])) {
      $cityref = isset($items[$delta]->city_ref) ? $items[$delta]->city_ref : NULL;
    }
    $options = [];
    if ($cityref) {
      $warehouses = $this->npConnect->getWarehouses($cityref);
      foreach ($warehouses as $warehouse) {
        $options[$warehouse['Ref']] = '№ ' . $warehouse['Number'] . ' ' . $warehouse['ShortAddress'];
      }
    }


    //@TODO: Check if select2 module exists.
    $element['warehouse_ref'] = [
      '#type' => 'select2',
      '#title' => t('Warehouse'),
//      '#required' => TRUE,
      '#default_value' => isset($items[$delta]->warehouse_ref) ? $items[$delta]->warehouse_ref : NULL,
      '#empty_option' => '- Select City first -',
      '#options' => $options,
      '#prefix' => '<div id="edit-warehouse-' . $delta . '">',
      '#suffix' => '</div>',
      '#attributes' => [
        'style' => 'width: 31em;',
      ],
      '#states' => [
        'invisible' => [
          ':input[name="' . $field_name . '[' . $delta . '][type]"]' => ['value' => '2'],
        ],
        'required' => [
          ':input[name="' . $field_name . '[' . $delta . '][type]"]' => [
            ['value' => '0'],
            ['value' => '1'],
          ],
        ],
      ],
    ];

    return $element;
  }

  /**
   * AJAX handler for warehouses select list.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return mixed Form element.
   */
  public function ajaxUpdateWarehouses(
    array &$form, FormStateInterface $form_state) {
    $trigger = $form_state->getTriggeringElement();
    $element = NestedArray::getValue($form, array_slice($trigger['#array_parents'], 0, -2));
    $i = 1;
    return $element[0]['warehouse_ref'];
  }

}
