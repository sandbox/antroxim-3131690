<?php

namespace Drupal\commerce_np\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'nova_poshta_address_type' field type.
 *
 * @FieldType(
 *   id = "nova_poshta_address_type",
 *   label = @Translation("Nova poshta address"),
 *   description = @Translation("Nova Poshta address field"),
 *   default_widget = "nova_poshta_address_widget",
 *   default_formatter = "nova_poshta_address_formatter"
 * )
 */
class NovaPoshtaAddressFieldType extends FieldItemBase {


  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    // Prevent early t() calls by using the TranslatableMarkup.
    $properties['type'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Delivery type.'));

    $properties['first_name'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('First name.'));

    $properties['last_name'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Last name.'));

    $properties['phone_number'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Phone number.'));

    $properties['city_name'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('City name.'));

    $properties['street_name'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Street.'));

    $properties['house_num'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('House number.'));

    $properties['apt_num'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Apartment.'));

    $properties['city_ref'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('City ref.'));

    $properties['warehouse_ref'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Warehouse ref.'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $schema = [
      'columns' => [
        'type' => [
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'default' => 0,
        ],
        'first_name' => [
          'type' => 'varchar',
          'length' => 32,
        ],
        'last_name' => [
          'type' => 'varchar',
          'length' => 32,
        ],
        'phone_number' => [
          'type' => 'varchar',
          'length' => 32,
        ],
        'city_name' => [
          'type' => 'varchar',
          'length' => 60,
        ],
        'city_ref' => [
          'type' => 'varchar',
          'length' => 50,
        ],
        'street_name' => [
          'type' => 'varchar',
          'length' => 60,
        ],
        'house_num' => [
          'type' => 'varchar',
          'length' => 10,
        ],
        'apt_num' => [
          'type' => 'varchar',
          'length' => 10,
        ],
        'warehouse_ref' => [
          'type' => 'varchar',
          'length' => 50,
        ],
      ],
    ];

    return $schema;
  }


  /**
   * {@inheritdoc}
   */
  public static function mainPropertyName() {
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('first_name');
    return $value === NULL || $value === '';
  }

}
